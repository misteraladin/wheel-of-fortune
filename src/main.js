import Vue from 'vue'

// COMPONENT
import App from './App.vue'

// MODULE
import VueAnime from 'vue-animejs';

// USR MODULE
Vue.use(VueAnime)

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
